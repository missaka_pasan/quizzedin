<html>
<head>
	<title>Login Page - Index</title>

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://code.getmdl.io/1.1.3/material.indigo-pink.min.css">
	<script defer src="https://code.getmdl.io/1.1.3/material.min.js"></script>

	<style type="text/css">
		#loginform{
			padding-top: 10%;
		}
	</style>

</head>

<body>

<div class="mdl-layout__header" id="header" align="center">
	<h2>Welcome to QuizzedIn</h2>

</div>

	<div align="center" id="loginform">
		<form action="Login.php" method="post" autocomplete="off">

			<div class="mdl-textfield mdl-js-textfield">
				<i class="material-icons">account_circle</i>
    			<input class="mdl-textfield__input" type="text" id="sample1" name="username">
    			<label class="mdl-textfield__label" for="sample1">Username</label>
  			</div>
  			<br>
  			<div class="mdl-textfield mdl-js-textfield">
  				<i class="material-icons">lock</i>
    			<input class="mdl-textfield__input" type="password" id="sample1" name="password">
    			<label class="mdl-textfield__label" for="sample1">Password</label>
  			</div>
			<br>
			<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Login <i class="material-icons">thumb_up</i></button>
			<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" formaction="signup.php">Sign Up</button>

			
			<!-- <input type="submit" name="Submit" value="Log In"> -->

		
		
		</form>

	</div>	

	

	

</body>
</html>